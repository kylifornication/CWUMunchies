$(document).on("pageshow","#map_page",function(){
	initialize();
	calculateRoute();
});
$(document).on("click","#submit",function(e){
	e.preventDefault();
	$("#theDirections").empty();
	calculateRoute();
	$.mobile.changePage("#map_page");
});

var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var map;

function initialize() {
	directionsDisplay = new google.maps.DirectionsRenderer();
	var mapCenter = new google.maps.LatLng(47, -120.5);

	var mapOptions = {
		zoom: 7,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		center: mapCenter,
	}
	map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
	directionsDisplay.setMap(map);
	directionsDisplay.setPanel(document.getElementById('theDirections'));
}

function calculateRoute() {
	var selectedMode = $('#mode').val(),
	start = $('#from').val(),
	end = $('#to').val();

	if(start=='' || end=='')
	{//cannot calculate the route!!!
	$('#theDirections').append('<h4><center>WARNING: You must enter a "TO" and "FROM" location. Please press back.<center></h4>');

	return;
	}
	else {
		var request = {
			origin: start,
			destination: end,
			travelMode: google.maps.DirectionsTravelMode[selectedMode]};
			directionsService.route(request,
			function(response,status){

				if(status==google.maps.DirectionsStatus.OK){
					directionsDisplay.setDirections(response);
					$('#theDirections').show();
				}
				else {
					$('#theDirections').hide();
				}
			});
	}
}
google.maps.event.addDomListener(window,'onload',initialize)
