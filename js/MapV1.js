// JavaScript Document
// Define your locations: HTML content for the info window, latitude, longitude
    var locations = [
	['<h4>Palace Cafe</h4><p><h4><a href="https://www.google.com/maps/dir/Current+Location/323+North+Main+Street+Ellensburg+WA+98926">Find Directions</a></p><p><a href="tel://1-509-925-2327">509-925-2327</a></h4></p><img src="media/Palace.jpg" height=50px width=100px>',
	 	46.995270, -120.548821],
	['<h4>Dominos</h4><p><h4><a href="https://www.google.com/maps/dir/Current+Location/710+North+Anderson+Street+Ellensburg+WA+98926">Find Directions</a></p><p><a href="tel://1-509-925-3800">509-925-3800</a></h4></p><img src="media/Dominos.jpg" height=50px width=100px>',
	 	46.999514, -120.539010],
	['<h4>Jack in the Box</h4><p><h4><a href="https://www.google.com/maps/dir/Current+Location/112+West+University+Way+Ellensburg+WA+98926">Find Directions</a></p><p><a href="tel://1-509-933-2833">509-933-2833</a></h4></p><img src="media/jackinthebox.jpg" height=50px width=100px>',
		 46.999760, -120.548548],
	['<h4>Pizza Hut</h4><p><h4><a href="https://www.google.com/maps/dir/Current+Location/805+North+A+Street+Ellensburg+WA+98926">Find Directions</a></p><p><a href="tel://1-509-925-7888">509-925-7888</a></h4></p><img src="media/pizzahut.jpg" height=50px width=100px>',
	 	47.000173, -120.549261],
	['<h4>Wing Central</h4><p><h4><a href="https://www.google.com/maps/dir/Current+Location/1801+North+Walnut+Street+Ellensburg+WA+98926">Find Directions</a></p><p><a href="tel://1-509-962-5400">509-962-5400</a></h4></p><img src="media/WingsCentral.jpg" height=50px width=100px>',
	 	47.010997, -120.540125],
	['<h4>Hot Diggity Dog</h4><p><h4><a href="https://www.google.com/maps/dir/Current+Location/115+West+3rd+Avenue+Ellensburg+WA+98926">Find Directions</a></h4></p><img src="media/Hotdog.jpg" height=50px width=100px>',
    46.994570, -120.548446],
	['<h4>The Tav</h4><p><h4><a href="https://www.google.com/maps/dir/Current+Location/117+West+4th+Avenue+Ellensburg+WA+98926">Find Directions</a></p><p><a href="tel://1-509-962-3939">509-962-3939</a></h4></p><img src="media/thetav.jpg" height=50px width=100px>',
		46.995592, -120.548301],
	['<h4>Taco Wagon</h4><p><h4><a href="https://www.google.com/maps/dir/Current+Location/209+South+Main+Street+Ellensburg+WA+98926">Find Directions</a></p><p><a href="tel://1-509-962-5643">509-962-5643</a></h4></p><img src="media/Tacowagon.jpg" height=50px width=100px>',
	 	46.995120, -120.548242],
	['<h4>Pizza Colin</h4><p><h4><a href="https://www.google.com/maps/dir/Current+Location/408+North+Main+Street+Ellensburg+WA+98926">Find Directions</a></p><p><a href="tel://1-509-925-7070">509-925-7070</a></h4></p><img src="media/pizzacolin.jpg" height=50px width=100px>',
	 	46.994897, -120.548510],
	['<h4>Pita Pit</h4><p><h4><a href="https://www.google.com/maps/dir/Current+Location/111+West+3rd+Avenue+Ellensburg+WA+98926">Find Directions</a></p><p><a href="tel://1-509-925-7482">509-925-7482</a></h4></p><img src="media/Pitapit.jpg" height=50px width=100px>',
	 	46.994544, -120.548046],
	['<h4>Campus 7-11</h4><p><h4><a href="https://www.google.com/maps/dir/Current+Location/112+West+University+Way+Ellensburg+WA+98926">Find Directions</a></p><p><a href="tel://1-509 925-3483">509 925-3483</a></h4></p><img src="media/seveneleven(1).jpg" height=50px width=100px>',
	 	47.002381, -120.533200],
	['<h4>Main 7-11</h4><p><h4><a href="https://www.google.com/maps/dir/Current+Location/1001+East+University+Way+Ellensburg+WA+98926">Find Directions</a></p><p><a href="tel://1-509-925-1605">509-925-1605</a></h4></p><img src="media/seveneleven(2).jpg" height=50px width=100px>',
	 	46.999309, -120.548397],
    ];

    // Setup the different icons and shadows
    var iconURLPrefix = 'http://maps.google.com/mapfiles/ms/icons/';

    var icons = [
      iconURLPrefix + 'red-dot.png',
      iconURLPrefix + 'green-dot.png',
      iconURLPrefix + 'blue-dot.png',
      iconURLPrefix + 'orange-dot.png',
      iconURLPrefix + 'purple-dot.png',
      iconURLPrefix + 'pink-dot.png',
      iconURLPrefix + 'yellow-dot.png'
    ]
    var iconsLength = icons.length;

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: new google.maps.LatLng(46.996514, -120.547847),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      streetViewControl: false,
      panControl: false,
      zoomControlOptions: {
         position: google.maps.ControlPosition.LEFT_BOTTOM
      }
    });

    var infowindow = new google.maps.InfoWindow({
      maxWidth: 140
    });

    var markers = new Array();

    var iconCounter = 0;

    // Add the markers and infowindows to the map
    for (var z = 0; z < locations.length; z++) {
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[z][1], locations[z][2]),
        map: map,
        icon: icons[iconCounter]
      });

      markers.push(marker);

      google.maps.event.addListener(marker, 'click', (function(marker, z) {
        return function() {
          infowindow.setContent(locations[z][0]);
          infowindow.open(map, marker);
        }
      })(marker, z));

      iconCounter++;
      // We only have a limited number of possible icon colors, so we may have to restart the counter
      if(iconCounter >= iconsLength) {
      	iconCounter = 0;
      }
    }

    function autoCenter() {
      //  Create a new viewpoint bound
      var bounds = new google.maps.LatLngBounds();
      //  Go through each...
      for (var z = 0; z < markers.length; z++) {
				bounds.extend(markers[z].position);
      }
      //  Fit these bounds to the map
      map.fitBounds(bounds);
    }
    autoCenter();
