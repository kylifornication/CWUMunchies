// JavaScript Document
$(document).on("pagecreate",function(){

	$.ajax({
		url:"calendar/sept2015.xml",
		cache:false,
		dataType:"xml",
		success: function(xml){
			$('#calendar').empty();

			$(xml).find('sept2015').each(function(){
				var info=  '<li data-id='+$(this).find("ID").text()
				+'><img src=media/'+$(this).find("Pic").text()
				+'><h1>'+$(this).find("Date").text()
				+'</h1><h3>'+$(this).find("Venue").text()
				+'</h3><p>'+$(this).find("Deal").text()
				+'TBD</p><p>'+$(this).find("Time").text()
				+'</p>'
				+'</li>';
				$('#calendar').append(info).listview('refresh');
			});
		}
	});
});
