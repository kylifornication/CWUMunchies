// JavaScript Document
$(document).on("pagecreate", function(){

	$.ajax({
		url:"courier/CourierDay.xml",
		cache:false,
		dataType:"xml",
		success: function(xml){
			$('#CourierDay').empty();

			$(xml).find('CourierDay').each(function(){
				var info=  '<li data-id='+$(this).find("ID").text()
				+'><a href=tel://'+$(this).find("Phone").text()
				+'><img src=media/'+$(this).find("Pic").text()
				+'><h1>'+$(this).find("Name").text()
				+'</h1>'
				+'<p>Delivery Loc.: '+$(this).find("DeliveryLoc").text()
				+'</p><p>Hours: '+$(this).find("Availability").text()
				+'</p><p>Ride: ' +$(this).find("Ride").text()
				+'</p><p>Tap to Call!</p></a></li>';
				$('#CourierDay').append(info).listview('refresh');

			});
		}
	});

	$.ajax({
		url:"courier/CourierEvening.xml",
		cache:false,
		dataType:"xml",
		success: function(xml){
			$('#CourierEvening').empty();

			$(xml).find('CourierEvening').each(function(){
				var info=  '<li data-id='+$(this).find("ID").text()
				+'><a href=tel://'+$(this).find("Phone").text()
				+'><img src=media/'+$(this).find("Pic").text()
				+'><h1>'+$(this).find("Name").text()
				+'</h1>'
				+'<p>Delivery Location: '+$(this).find("DeliveryLoc").text()
				+'</p><p>Hours: '+$(this).find("Availability").text()
				+'</p><p>Ride: ' +$(this).find("Ride").text()
				+'</p><p>Tap to Call!</p></a></li>';
				$('#CourierEvening').append(info).listview('refresh');

			});
		}
	});

	$.ajax({
		url:"courier/CourierNight.xml",
		cache:false,
		dataType:"xml",
		success: function(xml){
			$('#CourierNight').empty();

			$(xml).find('CourierNight').each(function(){
				var info=  '<li data-id='+$(this).find("ID").text()
				+'><a href=tel://'+$(this).find("Phone").text()
				+'><img src=media/'+$(this).find("Pic").text()
				+'><h1>'+$(this).find("Name").text()
				+'</h1>'
				+'<p>Delivery Location: '+$(this).find("DeliveryLoc").text()
				+'</p><p>Hours: '+$(this).find("Availability").text()
				+'</p><p>Ride: ' +$(this).find("Ride").text()
				+'</p><p>Tap to Call!</p></a></li>';
				$('#CourierNight').append(info).listview('refresh');

			});
		}
	});


});
